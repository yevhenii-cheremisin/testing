import CartParser from "./CartParser";
import { readFileSync } from "fs";

let parser, parse, validate, parseLine;

const path = "samples/cart.csv";
beforeEach(() => {
  parser = new CartParser();
  parse = parser.parse;
  validate = parser.validate;
  parseLine = parser.parseLine;
});

describe("CartParser - unit tests", () => {
  const CorrectData = `Product name,Price,Quantity
                      Mollis consequat,9.00,2`;
  const InvalidHeader = `Product,Price,Quantity
                      Mollis consequat,9.00,2`;
  const InvalidLineLength = `Product name,Price,Quantity
                      Mollis, consequat,9.00,2`;
  const InvalidLineValidation = `Product name,Price,Quantity
                      ,9.00,2`;
  const WrongNumberLessZero = `Product name,Price,Quantity
                      Mollis consequat, -3,2`;
  const WrongNumberText = `Product name,Price,Quantity
                      Mollis consequat, text,2`;
  const WrongNumberAnotherType = `Product name,Price,Quantity
                      Mollis consequat, ${NaN},2`;

  it("should run successfully when correct data", () => {
    expect(parser.parse(path)).not.toBe(Error);
  });

  it("should return empty array when correct content passed", () => {
    expect(parser.validate(CorrectData).length).toEqual(0);
  });

  it("should return not empty array when header is invalid", () => {
    expect(parser.validate(InvalidHeader).length).toBeGreaterThanOrEqual(1);
  });

  it("should return not empty array when rows has invalid length", () => {
    expect(parser.validate(InvalidLineLength).length).toBeGreaterThanOrEqual(1);
  });

  it("should return not empty array when nonempty string received", () => {
    expect(
      parser.validate(InvalidLineValidation).length
    ).toBeGreaterThanOrEqual(1);
  });

  it("should return not empty array when positive number received", () => {
    expect(parser.validate(WrongNumberLessZero).length).toBeGreaterThanOrEqual(
      1
    );
  });

  it("should return not empty array when text as number received", () => {
    expect(parser.validate(WrongNumberText).length).toBeGreaterThanOrEqual(1);
  });

  it("should return not empty array when another type as number received", () => {
    expect(
      parser.validate(WrongNumberAnotherType).length
    ).toBeGreaterThanOrEqual(1);
  });

  const itemList = [
    {
      id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
      name: "Tvoluptatem",
      price: 10.32,
      quantity: 1,
    },
    {
      id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
      name: "Mollis consequat",
      price: 9,
      quantity: 2,
    },
  ];

  const totalSum = 28.32;

  it("should return total sum of the list of items when list of items passed", () => {
    expect(parser.calcTotal(itemList)).toEqual(totalSum);
  });

  const itemLine = `Condimentum aliquet,13.90,1`;
  it("should return object with correct data when the line has passed", () => {
    expect(parser.parseLine(itemLine).price).toEqual(13.9);
    expect(parser.parseLine(itemLine).quantity).toEqual(1);
    expect(parser.parseLine(itemLine).name).toEqual("Condimentum aliquet");
  });
});

describe("CartParser - integration test", () => {
  // Add your integration test here.
  it("should return data when function calls", () => {
    expect(parser.readFile(path)).not.toBeUndefined();
  });
});
